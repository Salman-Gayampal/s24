// alert("Hello Salman!")

// S24 Activity Template:
/*Item 1.)
    - Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
    - Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
    */

// Code here:

let getCube = 6 ** 3
console.log(`The cube of 6 is ${getCube}`)



/*Item 2.)
    - Create a variable address with a value of an array containing details of an address.
    - Destructure the array and print out a message with the full address using Template Literals.*/


// Code here:
const address = [
    "Block 57", "Lot 49", "Juan Luna Street", "Brgy. Meow", "Lost City"
]

const [block, lot, street, barangay, city] = address

console.log(`I am situated in ${block} ${lot} ${street}, ${barangay}, ${city}. And you?`)

/*Item 3.)
    - Create a variable animal with a value of an object data type with different animal details as its properties.
    - Destructure the object and print out a message with the details of the animal using Template Literals.
*/
// Code here:

const animal = {
    animalType: "Lion",
    animalName: "Simba",
    age: 35
}

const { animalType, animalName, age } = animal
console.log(`My favorite animal is a ${animalType}. His name is ${animalName} and he is ${age} years old.`)


/*Item 4.)
    - Create an array of numbers.
    - Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

// Code here:

const randomNum = [100, 200, 300, 400, 500]

randomNum.forEach((num) => {
    console.log(`${num}`)
})


/*
    Item 5.)
        - Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
        - Create/instantiate a new object from the class Dog and console log the object.*/

// Code here:

class Dog {
    constructor(name, age, breed) {
        this.name = name
        this.age = age
        this.breed = breed
    }
}
const myDog = new Dog("Nyark", "57", "Siberian Husky")
console.log(myDog)